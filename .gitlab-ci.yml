---
stages:
  - build_dev
  - deploy_dev

variables:
  APP_HOSTNAME: $APP_HOSTNAME
  APP_PORT: $APP_PORT
  AZURE_ACCESS_KEY: $AZURE_ACCESS_KEY
  AZURE_ACCOUNT_NAME: $AZURE_ACCOUNT_NAME
  AZURE_CONTAINER_NAME: $AZURE_CONTAINER_NAME
  HLS_FOLDER: $HLS_FOLDER
  LOAD_BALANCER_PUBLIC_IP: $LOAD_BALANCER_PUBLIC_IP
  NGINX_PORT: 5050
  NGINX_STATUS_PORT: 2221
  POSTGRESQL_HOST: $LOAD_BALANCER_PUBLIC_IP
  POSTGRESQL_PORT: 5432
  POSTGRESQL_USERNAME: postgres
  POSTGRESQL_PASSWORD: postgres
  POSTGRESQL_DB: appstream
  PUMA_PORT: 5000
  REDIS_HOST: $LOAD_BALANCER_PUBLIC_IP
  REDIS_PORT: 6379
  REDIS_DB: 1
  SECRET_KEY_BASE: $SECRET_KEY_BASE

.docker:
  image: docker:latest
  services:
    - name: docker:dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$IMAGE_NAME
  before_script:
    - echo $CI_JOB_TOKEN | docker login -u gitlab-ci-token registry.gitlab.com --password-stdin

.azure:
  image: mcr.microsoft.com/azure-cli
  before_script:
    - az login --service-principal -u $CLIENT_ID -p $CLIENT_SECRET -t $TENANT_ID
    - az aks install-cli
    - az aks get-credentials --resource-group $RESOURCE_GROUP_NAME --name $CLUSTER_NAME
    - kubectl apply -f .gitlab/azure/namespace.yml
    - kubectl create secret docker-registry gitlab-credentials --docker-server="$CI_REGISTRY" --docker-username="$CI_DEPLOY_USER" --docker-password="$CI_DEPLOY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" -n development --dry-run=client -o yaml | kubectl apply -f -

build-postgresql:
  stage: build_dev
  extends: .docker
  variables:
    IMAGE_NAME: postgresql
  script:
    - docker build . -t $IMAGE_TAG --build-arg postgresql_port="$POSTGRESQL_PORT" -f .gitlab/docker/postgresql/Dockerfile
    - docker push $IMAGE_TAG

build-redis:
  stage: build_dev
  extends: .docker
  variables:
    IMAGE_NAME: redis
  script:
    - docker build . -t $IMAGE_TAG --build-arg redis_port="$REDIS_PORT" -f .gitlab/docker/redis/Dockerfile
    - docker push $IMAGE_TAG

build-app:
  stage: build_dev
  extends: .docker
  variables:
    IMAGE_NAME: app-stream
    ENV_NAME: development
  script:
    - docker build . -t $IMAGE_TAG --build-arg hls_folder="$HLS_FOLDER" --build-arg puma_port="$PUMA_PORT" --build-arg env_name="$ENV_NAME" -f .gitlab/docker/app/Dockerfile
    - docker push $IMAGE_TAG

build-nginx:
  stage: build_dev
  extends: .docker
  variables:
    IMAGE_NAME: nginx-stream
    ENV_NAME: development
  script:
    - docker build . -t $IMAGE_TAG -f .gitlab/docker/nginx/Dockerfile --build-arg nginx_port="$NGINX_PORT" --build-arg nginx_status_port="$NGINX_STATUS_PORT"
    - docker push $IMAGE_TAG

deploy-postgresql:
  stage: deploy_dev
  extends: .azure
  script:
    - kubectl -n development replace --force -f .gitlab/azure/postgresql/deployment.yml
    - kubectl -n development replace --force -f .gitlab/azure/postgresql/service.yml
    - kubectl -n development patch svc postgresql-load-balancer -p '{"spec":{"externalIPs":["'${LOAD_BALANCER_PUBLIC_IP}'"]}}'
  rules:
    - when: manual

deploy-redis:
  stage: deploy_dev
  extends: .azure
  script:
    - kubectl -n development replace --force -f .gitlab/azure/redis/deployment.yml
    - kubectl -n development replace --force -f .gitlab/azure/redis/service.yml
    - kubectl -n development patch svc redis-load-balancer -p '{"spec":{"externalIPs":["'${LOAD_BALANCER_PUBLIC_IP}'"]}}'
  rules:
    - when: manual

deploy-app:
  stage: deploy_dev
  extends: .azure
  script:
    - cp .gitlab/docker/app/config/.env.dev /tmp/.env
    - sed -i "s~__APP_HOSTNAME__~${APP_HOSTNAME}~" /tmp/.env
    - sed -i "s~__APP_PORT__~${APP_PORT}~" /tmp/.env
    - sed -i "s~__AZURE_ACCOUNT_NAME__~${AZURE_ACCOUNT_NAME}~" /tmp/.env
    - sed -i "s~__AZURE_CONTAINER_NAME__~${AZURE_CONTAINER_NAME}~" /tmp/.env
    - sed -i "s~__AZURE_ACCESS_KEY__~${AZURE_ACCESS_KEY}~" /tmp/.env
    - sed -i "s~__HLS_FOLDER__~${HLS_FOLDER}~" /tmp/.env
    - sed -i "s~__POSTGRESQL_HOST__~${POSTGRESQL_HOST}~" /tmp/.env
    - sed -i "s~__POSTGRESQL_PORT__~${POSTGRESQL_PORT}~" /tmp/.env
    - sed -i "s~__POSTGRESQL_USERNAME__~${POSTGRESQL_USERNAME}~" /tmp/.env
    - sed -i "s~__POSTGRESQL_PASSWORD__~${POSTGRESQL_PASSWORD}~" /tmp/.env
    - sed -i "s~__POSTGRESQL_DB__~${POSTGRESQL_DB}~" /tmp/.env
    - sed -i "s~__REDIS_HOST__~${REDIS_HOST}~" /tmp/.env
    - sed -i "s~__REDIS_PORT__~${REDIS_PORT}~" /tmp/.env
    - sed -i "s~__REDIS_DB__~${REDIS_DB}~" /tmp/.env
    - sed -i "s~__SECRET_KEY_BASE__~${SECRET_KEY_BASE}~" /tmp/.env
    - kubectl create secret generic app-stream-env --namespace=development --from-file=/tmp/.env --dry-run=client -o yaml | kubectl -n development apply -f - 
    - kubectl -n development replace --force -f .gitlab/azure/app/deployment.yml
    - kubectl -n development replace --force -f .gitlab/azure/app/service.yml
    - kubectl -n development patch svc app-stream-load-balancer -p '{"spec":{"externalIPs":["'${LOAD_BALANCER_PUBLIC_IP}'"]}}'
  rules:
    - when: manual

deploy-nginx:
  stage: deploy_dev
  extends: .azure
  script:
    - kubectl -n development replace --force -f .gitlab/azure/nginx/deployment.yml
    - kubectl -n development replace --force -f .gitlab/azure/nginx/service.yml
    - kubectl -n development patch svc nginx-stream-load-balancer -p '{"spec":{"externalIPs":["'${LOAD_BALANCER_PUBLIC_IP}'"]}}'
  rules:
    - when: manual
