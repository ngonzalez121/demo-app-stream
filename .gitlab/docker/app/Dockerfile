FROM ruby:3.3.0-bookworm

# apt
RUN echo 'deb http://ftp.fr.debian.org/debian/ unstable main contrib non-free' > /etc/apt/sources.list
RUN apt-get update -yqq

# USER
ARG user="appuser"
ENV USER $user
RUN echo "USER=$USER" > /etc/profile.d/user.sh
RUN useradd -m $USER -s /bin/bash
RUN apt-get install -yqq sudo
RUN echo "$USER ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers

# entrypoint
COPY .gitlab/docker/app/entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

WORKDIR /app

COPY ../../../backend .

RUN chown -R $USER: /app

ARG env_name="development"
ENV RAILS_ENV $env_name

ARG RAILS_LOG_TO_STDOUT="true"
ENV RAILS_LOG_TO_STDOUT $RAILS_LOG_TO_STDOUT

# apt dependencies
RUN apt-get install -yq ffmpeg lame libtag1-dev

# bundler
ARG BUNDLER_VERSION="2.5.6"
ENV BUNDLER_VERSION $BUNDLER_VERSION
RUN su -c "gem install bundler --no-document -v $BUNDLER_VERSION" $USER
RUN su -c "bundle install" $USER

# hls folder
ARG hls_folder
ENV HLS_FOLDER = $hls_folder
RUN mkdir -p $HLS_FOLDER
RUN chown $USER $HLS_FOLDER
RUN chmod 0755 $HLS_FOLDER

ENTRYPOINT []

ARG puma_port="5000"
ENV PUMA_PORT $puma_port

EXPOSE $PUMA_PORT
CMD ["/usr/bin/entrypoint.sh", "--server"]
