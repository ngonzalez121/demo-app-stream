#!/usr/bin/env bash
set -e

function start_nginx {
    nginx -g 'daemon off;';
}

function usage {
    echo "usage: entrypoint.sh [--server|--help]"
}

if [ "$1" != "" ]; then
    case $1 in
        -s | --server )   start_nginx
                          exit
                          ;;
        -h | --help )     usage
                          exit
                          ;;
    esac
    shift
fi

exec "$@"
