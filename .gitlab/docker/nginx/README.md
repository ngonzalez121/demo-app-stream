##### demo-app / nginx

Generate SSL certificates:

```shell
sudo certbot certonly --manual
```

Validation: Add Acme challenge to NGINX config

```nginx
location /.well-known/acme-challenge {
  alias /var/www/public/.well-known/acme-challenge/;
}
```

Generate dhparams file:

```shell
openssl dhparam -out dhparams.pem 4096
```

Validate from openssl client:

```shell
openssl s_client -showcerts -connect <hostname>:443
```
