FROM debian:bookworm

# apt
ENV DEBIAN_FRONTEND noninteractive
RUN echo "deb http://ftp.fr.debian.org/debian/ bookworm main contrib non-free" > /etc/apt/sources.list
RUN apt-get update -yq
RUN apt-get dist-upgrade -yq
RUN apt-get upgrade -yq

# debconf
RUN apt-get install -yq debconf libreadline-dev
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN dpkg-reconfigure debconf

# USER
ARG user="www-data"
ENV USER $user

# NGINX_PORT
ARG nginx_port
ENV NGINX_PORT $nginx_port

# NGINX_STATUS_PORT
ARG nginx_status_port
ENV NGINX_STATUS_PORT $nginx_status_port

# Entrypoint
COPY .gitlab/docker/nginx/entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

# nginx
RUN apt-get update && apt-get install -yqq nginx-dev

# SSL certificate
COPY .gitlab/docker/nginx/certs/fullchain.pem /etc/ssl/certs/bundle.crt
RUN chmod 0644 /etc/ssl/certs/bundle.crt
RUN chown $USER: /etc/ssl/certs/bundle.crt

# SSL private key
COPY .gitlab/docker/nginx/certs/privkey.pem /etc/ssl/private/bundle-key.pem
RUN chmod 0644 /etc/ssl/private/bundle-key.pem
RUN chown $USER: /etc/ssl/private/bundle-key.pem

# SSL dhparams
COPY .gitlab/docker/nginx/certs/dhparams.pem /etc/nginx/dhparams.pem
RUN chmod 0644 /etc/nginx/dhparams.pem
RUN chown $USER: /etc/nginx/dhparams.pem

# nginx ssl.conf
COPY .gitlab/docker/nginx/config/ssl.conf /etc/nginx/snippets/ssl.conf
RUN chmod 0644 /etc/nginx/snippets/ssl.conf
RUN chown $USER: /etc/nginx/snippets/ssl.conf

# nginx ssl-params.conf
COPY .gitlab/docker/nginx/config/ssl-params.conf /etc/nginx/snippets/ssl-params.conf
RUN chmod 0644 /etc/nginx/snippets/ssl-params.conf
RUN chown $USER: /etc/nginx/snippets/ssl-params.conf

# nginx nginx.conf
COPY .gitlab/docker/nginx/config/nginx.conf /etc/nginx/nginx.conf
RUN chmod 0644 /etc/nginx/nginx.conf
RUN chown $USER: /etc/nginx/nginx.conf

# nginx nginx-app.conf
COPY .gitlab/docker/nginx/config/nginx-app.conf /etc/nginx/sites-enabled/nginx-app.conf
RUN sed -i "s/__NGINX_PORT__/$NGINX_PORT/" /etc/nginx/sites-enabled/nginx-app.conf
RUN chmod 0644 /etc/nginx/sites-enabled/nginx-app.conf
RUN chown $USER: /etc/nginx/sites-enabled/nginx-app.conf
RUN rm -f /etc/nginx/sites-enabled/default

# nginx nginx-status.conf
COPY .gitlab/docker/nginx/config/nginx-status.conf /etc/nginx/sites-enabled/nginx-status.conf
RUN sed -i "s/__NGINX_STATUS_PORT__/$NGINX_STATUS_PORT/" /etc/nginx/sites-enabled/nginx-status.conf
RUN chmod 0644 /etc/nginx/sites-enabled/nginx-status.conf
RUN chown $USER: /etc/nginx/sites-enabled/nginx-status.conf

EXPOSE $NGINX_PORT

STOPSIGNAL SIGQUIT

CMD ["/usr/bin/entrypoint.sh", "--server"]
